<?php

namespace LDL\Whise;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Config\Repository;


class WhiseAPI
{

    /** @var array */
    protected $config;

    /** @var string */
    private const BASE_URI = 'https://api.whise.eu/';

    /** @var string */
    private $accessToken;

    public function __construct(Repository $config)
    {
        $this->config = $config['access_token'];
    }

    public function getAccessToken():string
    {
        if(empty($this->accessToken)) {
            throw new AuthenticationException('Your API key is inccorrect');
        }
        return $this->accessToken;
    }

    public function setAccessToken(string $config)
    {
        $this->accessToken = $config['access_token'];
        return $this;
    }

}
