<?php


namespace LDL\Whise\Api;


interface ApiConnectorInterface
{
    public function call(string $endpoint, array $data = []): array;
}
