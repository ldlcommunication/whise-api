<?php


namespace LDL\Whise\Api;


interface WhiseEndpointInterface
{
    public function fetchAll(): Collection;

    public function fetch(string $id): EntityInterface;
}
