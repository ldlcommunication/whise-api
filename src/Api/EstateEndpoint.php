<?php


namespace LDL\Whise\Api;


class EstateEndpoint implements WhiseEndpointInterface
{
    // Since this will be a string, it's best to put it in a constant in case the api url changes for some reason
    // This should not be placed in the interface as this url/uri is specific to the estate endpoint
    private const ENDPOINT_FETCH_ALL = '/v1/estates/all';

    private $apiConnector;
    private $mapper;

    // The interfaces should be type-hinted here. In the service provider there should be code that tells which implementing classes are passed here. ie. $this->app->when(EstateEndpoint::class)->needs(MapperInterface::class)->give(function () { return new EstateMapper; });
    public function __construct(ApiConnectorInterface $apiConnector, MapperInterface $mapper)
    {
        $this->apiConnector = $apiConnector;
        $this->mapper = $mapper;
    }


    public function fetch($id): EntityInterface
    {
        return "1";
    }

    public function fetchAll(): Collection
    {
        $data = $this->apiConnector->call(self::ENDPOINT_FETCH_ALL);
        $collection = collect();

        foreach ($data as $estate)
        {
            $collection->push(
                $this->mapper->map($estate)
            );
        }

        return $collection;
    }
}
