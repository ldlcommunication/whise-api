<?php


namespace LDL\Whise\Api;


interface EstateInterface extends EntityInterface
{
    public function getId(): string;
}
