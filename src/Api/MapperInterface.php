<?php


namespace LDL\Whise\Api;


interface MapperInterface
{
    public function map(array $data): EntityInterface;
}
