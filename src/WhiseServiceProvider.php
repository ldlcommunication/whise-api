<?php

namespace LDL\Whise;

use Illuminate\Support\ServiceProvider;
use LDL\Whise\Console\Commands\SyncEstatesCommand;

class WhiseServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Run migrations
        if ($this->app->runningInConsole()) {

            if (! class_exists('CreateEstatesTable')) {
                $this->publishes([
                    __DIR__ . '/../database/migrations/create_estates_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_estates_table.php'),
                ], 'migrations');
            }
        }

        $this->commands([
            SyncEstatesCommand::class,
        ]);

        include __DIR__.'/routes.php';
    }
}
