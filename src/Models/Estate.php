<?php


namespace LDL\Whise\Models;


use Illuminate\Database\Eloquent\Model;

class Estate extends Model
{
    protected $fillable = [
        'name'
    ];
}
