<?php

namespace LDL\Whise\Console\Commands;

use Illuminate\Console\Command;
use LDL\Whise\Models\Estate;
use LDL\Whise\WhiseAPI;
use LDL\Whise\WhiseServiceProvider;

class SyncEstatesCommand extends Command
{
    protected $signature = 'whise:sync';

    protected $description = 'Sync the new estates/properties with our database.';

    /**
     * The API repository instance.
     */
    protected $api;

    public function __construct(WhiseAPI $api)
    {
        parent::__construct();

        $this->api = $api;
    }

    public function handle()
    {
        $this->info('----- Running the command -----');

        //$this->info($this->api->getAccessToken());

        $estate = Estate::create([
            'name' => 'test',
        ]);

        dump($estate);

        $this->info('----- Installed the package -----');
    }
}
