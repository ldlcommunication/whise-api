
Optionally, you can publish the config file of this package with this command:

``` bash
php artisan vendor:publish --provider="LDL\Whise\WhiseServiceProvider"
```

Add the command to your application in Console/Kernel.php
```
SyncEstatesCommand::class
```
