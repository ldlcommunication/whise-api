<?php

return [
    /**
     * To work with the Whice API you need to get a Whise Access token*
     */
    'access_token' => env('WHISE_API_ACCESS_TOKEN'),
];
